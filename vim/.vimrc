"softtabs
set softtabstop=4 tabstop=4 shiftwidth=4 expandtab

"textwidth
"set textwidth=80
"set formatoptions+=t

"highlight right margin
let &colorcolumn=join(range(81,999),",")
"highlight right margin differently than colorscheme specifies
"vim behaves a bit strangle here. The colorscheme is somehow loaded *after* all
"the config is read. So it's not good enough to just do this:
"  highlight ColorColumn ctermbg=black guibg=black
"Instead, we need to make change triggered on a change of colorscheme.
"https://stackoverflow.com/questions/2440149/override-colorscheme
autocmd ColorScheme * highlight ColorColumn ctermbg=black guibg=black

"Tab completion for commands. Press tab once to complete to the next match and
"show the possible completions in menu. Press again to complete the first
"listed choice.
set wildmenu
set wildmode=longest:full,full

"pathogen
"https://github.com/tpope/vim-pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

"set a colorscheme
"https://github.com/altercation/vim-colors-solarized
set background=dark
"colorscheme solarized
colorscheme wombat256mod

if has('gui_running')
    set guifont=DejaVu\ Sans\ Mono\ 8

    "disable menubar
    set guioptions -=m
    "disable toolbar
    set guioptions -=T
endif

"Backspace/Delete on MacOS
set backspace=indent,eol,start
