cd into each directory and git clone the following URL:

(cd nerdcommenter; git clone https://github.com/scrooloose/nerdcommenter.git)
(cd vim-colors-solarized; git clone https://github.com/altercation/vim-colors-solarized.git)
(cd vim-sensible; git clone https://github.com/tpope/vim-sensible.git)
