#!/bin/bash
TMP=~/tmp
mkdir -p $TMP

SETUP_EXE="setup-x86.exe"
if [[ "$(uname -m)" =~ "x86_64" ]]; then
    SETUP_EXE="setup-x86_64.exe"
fi

(cd $TMP; wget https://cygwin.com/${SETUP_EXE})
chmod a+x $TMP/${SETUP_EXE}
$TMP/${SETUP_EXE} --no-admin
