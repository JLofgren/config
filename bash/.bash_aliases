# enable color support of ls and also add handy aliases
#if [ -x /usr/bin/dircolors ]; then
    #test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    #alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
#fi

case "$(uname -s)" in
    Darwin*)
        alias ls='ls -hFG'
        ;;
    *)
        alias ls='ls -hF --color=tty'
        ;;
esac

#alias ls='ls -hF --color=tty'                 # classify files in colour
alias ll='ls -l'
alias la='ls -Al'
alias lla='ls -al'



alias x=exit
alias i3-config='vim ~/.config/i3/config'
alias duall="du -sk .[!.]* *"


#Git
alias gs="git status"
alias gl='git log --all --graph --pretty=format:"%C(auto)%h %<(12,trunc)%an %<(12,trunc)%ar %d %s"'


#OpenSSL
alias ssl-x509-text="openssl x509 -text -noout -in"
alias ssl-req-text="openssl req -text -noout -in"
alias ssl-client="openssl s_client -connect"


#This repository file for Chrome Remote Desktop keeps getting reset
#periodically by something. The [arch=amd64] is not there by default, but it
#needs to be, otherwise an error from 'apt-get update'
alias fix-apt-chrome-remote-desktop="sudo sed --in-place -e 's/^deb http/deb \[arch=amd64\] http/' /etc/apt/sources.list.d/chrome-remote-desktop.list"

#Fix dropbox icon not responding to click
alias fix-dropbox='dropbox stop && dbus-launch dropbox start'

