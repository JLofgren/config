# .bashrc

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

#Things to source before the body of this file
bash_sources_before="/etc/bashrc"
#Things to source before the body of this file in Cygwin
bash_sources_before_cygwin=""
#Things to source after the body of this file
bash_sources_after=" \
    ${HOME}/.bash_aliases \
    ${HOME}/.bash_aliases.local \
    ${HOME}/.bash_proxy \
    ${HOME}/.bashrc.local \
    ${HOME}/.tmuxinator.bash \
    ${HOME}/.bash_prompt \
    ${HOME}/.bash_prompt.local \
    "
#Things to source after the body of this file in Cygwin
bash_sources_after_cygwin=" \
    ${HOME}/.bash_cygwin \
    ${HOME}/.bash_cygwin.local \
    "

function source_files {
    files=$@
    for f in $files; do
        if [ -e $f ]; then
            echo "--- SOURCING $f ---"
            source $f
        else
            echo "--- NOT FOUND: $f ---"
        fi
    done
}

#Source things before the body
source_files $bash_sources_before
[ $OSTYPE == "cygwin" ] && source_files $bash_sources_before_cygwin

##############################################################################
# BODY

# Don't use ^D to exit
set -o ignoreeof

#Standard PATH additions
export PATH=$PATH:~/bin
for d in ~/bin/bin-*; do
    export PATH=$PATH:$d
done

#Set vim as editor if it is installed
command -v vim && export EDITOR='vim'

#directory stacking
function mycd() {
	pushd $1 > /dev/null
}
alias cd=mycd
alias bk="pushd +1"
alias fd="pushd -0"

#cd up N directories
function up()
{
    dir=""
    if [ -z "$1" ]; then
        dir=..
    elif [[ $1 =~ ^[0-9]+$ ]]; then
        x=0
        while [ $x -lt ${1:-1} ]; do
            dir=${dir}../
            x=$(($x+1))
        done
    else
        dir=${PWD%/$1/*}/$1
    fi
    cd "$dir";
}

#Print command string, then evaluate it
function eval-verb {
    ## print the command
    echo "$@"
    ## run the command
    eval "$@" 
}


#colors
txtblk='\e[0;30m' # Black - Regular
txtred='\e[0;31m' # Red
txtgrn='\e[0;32m' # Green
txtylw='\e[0;33m' # Yellow
txtblu='\e[0;34m' # Blue
txtpur='\e[0;35m' # Purple
txtcyn='\e[0;36m' # Cyan
txtwht='\e[0;37m' # White
bldblk='\e[1;30m' # Black - Bold
bldred='\e[1;31m' # Red
bldgrn='\e[1;32m' # Green
bldylw='\e[1;33m' # Yellow
bldblu='\e[1;34m' # Blue
bldpur='\e[1;35m' # Purple
bldcyn='\e[1;36m' # Cyan
bldwht='\e[1;37m' # White
unkblk='\e[4;30m' # Black - Underline
undred='\e[4;31m' # Red
undgrn='\e[4;32m' # Green
undylw='\e[4;33m' # Yellow
undblu='\e[4;34m' # Blue
undpur='\e[4;35m' # Purple
undcyn='\e[4;36m' # Cyan
undwht='\e[4;37m' # White
bakblk='\e[40m'   # Black - Background
bakred='\e[41m'   # Red
bakgrn='\e[42m'   # Green
bakylw='\e[43m'   # Yellow
bakblu='\e[44m'   # Blue
bakpur='\e[45m'   # Purple
bakcyn='\e[46m'   # Cyan
bakwht='\e[47m'   # White
txtrst='\e[0m'    # Text Reset


#Find all git repos under the working directory and run a command on each
function git-all {
    repos=$(find . -name .git | xargs dirname)
    for repo in $repos; do
        pushd $repo > /dev/null
        echo -e "${txtgrn}$repo${txtrst}"
        eval "$@"
        echo
        popd > /dev/null
    done
}

function for-cert-in {
    MULTI_CERT_FILE=$1
    shift
    CMD=$@

    TMPDIR=`mktemp -d`
    
    PAT="-----BEGIN.*"
    csplit --digits=4 --quiet --prefix=${TMPDIR}/cert $MULTI_CERT_FILE \
        "%${PAT}%" "/${PAT}/" "{*}"
    
    for cert in ${TMPDIR}/*; do
        ${CMD//\{\}/${cert}}
        echo
    done
    
    rm -rf $TMPDIR
}

function get-cert-n {
    awk -v n=$1 -e '
        /-----BEGIN/ { n-- }
        (n == 0)     { print }
        /-----END/   { if (n == 0) n-- }
    '
}

#Given a 'hostname:port' string, show the SSL cert of the server there
function ssl-server-cert-text {
    openssl s_client -connect $1 </dev/null | openssl x509 -text -noout
}



#########################################
#Stuff from Linux Mint 18's default bashrc

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

if [ -x /usr/bin/mint-fortune ]; then
     /usr/bin/mint-fortune
fi

#########################################


#Go
#export GOROOT=/usr/local/go
#export PATH=${GOROOT}/bin:${PATH}
#if command -v go; then
#    export GOPATH=$(go env GOPATH)
#    export PATH=${PATH};${GOPATH}/bin
#fi


# added by Pew - I am consciously overriding the pew prompt in my own format
if type pew > /dev/null 2>&1; then
    source $(pew shell_config)
fi


##############################################################################
# FOOTER

#Source things after the body
source_files $bash_sources_after
[ $OSTYPE == "cygwin" ] && source_files $bash_sources_after_cygwin

