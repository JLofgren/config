#!/bin/bash
#Make links to the listed FILES in HOME

MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MYDIR_REL_HOME=${MYDIR#${HOME}/}
FILES=" \
    bash/.bash_aliases \
    bash/.bash_cygwin \
    bash/.bash_prompt \
    bash/.bashrc \
    git/.gitconfig.include \
    git/.gitignore_global \
    tmux/.tmux.conf \
    tmux/.tmuxinator.bash \
    vim/.vim \
    vim/.vimrc \
    "

for f in $FILES; do
    target=${MYDIR_REL_HOME}/$f
    (cd $HOME; ln -s ${MYDIR_REL_HOME}/$f)
done


#For every file in source_dir, make a symlink to that file in dest_dir.
#For every directory in source_dir, make a directory to match in dest_dir.
function link_all_files {
    source_dir=$1
    dest_dir=$2

    echo source_dir=$source_dir
    echo dest_dir=$dest_dir

    [ ! -d $source_dir ] && echo "source_dir '$source_dir' does not exist" && return 1
    [ ! -d $dest_dir ] && echo "dest_dir '$dest_dir' does not exist" && return 1

    pushd $source_dir
    files_rel=$(find * -type f)
    dirs_rel=$(find * -type d)
    popd

    pushd $dest_dir
    echo "dirs:"
    for d in $dirs_rel; do
        mkdir -p $d
    done
    echo "files:"
    for f in $files_rel; do
        fdir=$(dirname $f)
        fname=$(basename $f)
        pushd $fdir
        ln -sf ${source_dir}/$f
        popd
    done
    popd
}

function ensure_dir {
    dir=$1
    [[ -e $dir && ! -d $dir ]] && echo "path '$dir' exists, but is not a directory" && return 1
    [[ ! -d $dir ]] && mkdir -p $dir
}


#i3 is different
#([ -d ~/.config/i3 ] && cd ~/.config/i3 && ln -s ${MYDIR}/i3/concat-config.sh)
if [ -d ~/.config/i3 ]; then
    link_all_files ${MYDIR}/i3 ~/.config/i3 || exit 1
    $MYDIR/i3/concat-config.sh
fi

#(cd ~/.config/i3status; ln -s ${MYDIR}/i3status/config)

ensure_dir ~/bin
link_all_files ${MYDIR}/bin ~/bin || exit 1

ensure_dir ~/.config/lilyterm
link_all_files ${MYDIR}/lilyterm ~/.config/lilyterm || exit 1


